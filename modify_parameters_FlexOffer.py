import numpy as np


# Modify Parameters from OpenTUM to fit with FlexOffer-format
def modify_params(OpenTUM_res, t_fac):
    params_mod = {}
    devices = OpenTUM_res['devices']

    for device in devices:
        # Define ev parameters
        if device == 'ev':
            process_params = {}
            params = devices[device]

            # Availability & Capacity parameters
            aval = np.array(params['aval'])
            aval[95] = 1.0
            aval[32] = 1.0
            c_init = np.array(params['initSOC'])
            c_min = np.array(params['endSOC'])
            c_max = np.array([100] * len(c_init))

            # Load rate r in [kWh / %SOC]
            r = np.array([params['stocap'] / 100] * len(c_init))

            # Base Energy demand for process and flexible Energy
            e_b = np.multiply(np.subtract(c_min, c_init), r)
            e_f = np.multiply(np.subtract(c_max, c_min), r)

            # Earliest starttime and latest endtime evaluation
            t_es = []
            t_le = []

            for t_step, ava in enumerate(aval):

                if t_step == 0:
                    t_es.append(t_step)

                elif ava == 1 and aval[t_step - 1] == 0:
                    t_es.append(t_step)

                if t_step == 95:
                    t_le.append(t_step)

                elif ava == 1 and aval[t_step + 1] == 0:
                    t_le.append(t_step)

            t_es = np.array(t_es)
            t_le = np.array(t_le)
            opt_plan = OpenTUM_res['optplan']['EV_power']

            # Extraction of process parameters from historical data for each load process
            for p_idx in range(len(t_es)):
                t_steps = []
                power = []
                phi = []

                ev_params = {'opt_plan': float(0), 't_steps': float(0),
                             'process_power': float(0), 'phi': float(0),
                             'process_duration': float(0), 't_es': float(0),
                             't_le': float(0)}

                for t_step in range(t_es[p_idx], t_le[p_idx] + 1):
                    if opt_plan[t_step] > 0:
                        t_steps.append(t_step)
                        power.append(opt_plan[t_step])
                        phi.append(opt_plan[t_step] * t_fac / (e_b[p_idx]))

                ev_params['opt_plan'] = opt_plan
                ev_params['t_steps'] = t_steps
                ev_params['process_power'] = power
                ev_params['phi'] = phi
                ev_params['process_duration'] = len(t_steps)
                ev_params['t_es'] = t_es[p_idx]
                ev_params['t_le'] = t_le[p_idx]
                ev_params['e_b'] = e_b[p_idx]
                ev_params['e_f'] = e_f[p_idx]

                process_params['FlexOffer_' + str(p_idx)] = ev_params
                params_mod[device] = process_params

        # Define heat pump parameters
        if device == 'hp':
            process_params = {}
            hp_params = {'hp_type': float(0), 'thermal_R': float(0),
                         'thermal_C': float(0), 'eta': float(0),
                         'T_min': float(0), 'T_max': float(0),
                         'T_amb': float(0), 'T_in': float(0)}

            # Heating room data
            hp_params['thermal_R'] = 7.5   # °C/kW
            hp_params['thermal_C'] = 0.3   # kWh/°C

            # Heat pump data
            hp_params['eta'] = list(OpenTUM_res['optplan']['HP_COP'])

            # Comfort constraints
            T_min = devices['hp']['minTemp']
            T_max = devices['hp']['maxTemp']
            hp_params['T_min'] = devices['hp']['minTemp']
            hp_params['T_max'] = devices['hp']['maxTemp']

            # Load ambient temperature data
            T_amb_dat = np.loadtxt("flex_models/FlexOffer/T_data/T_amb_H.txt")[:, 2]

            # Linear extension of ambient temperature data to 96*15min
            T_amb = []
            for t_step in range(len(T_amb_dat) - 1):
                T_amb.append(T_amb_dat[t_step])
                T_amb.append(T_amb_dat[t_step] + 1 * (T_amb_dat[t_step + 1] - T_amb_dat[t_step]) / 4)
                T_amb.append(T_amb_dat[t_step] + 2 * (T_amb_dat[t_step + 1] - T_amb_dat[t_step]) / 4)
                T_amb.append(T_amb_dat[t_step] + 3 * (T_amb_dat[t_step + 1] - T_amb_dat[t_step]) / 4)

            hp_params['T_amb'] = T_amb

            # Defintion of inital room temperature
            T_in = []
            T_in.append(((T_max - T_min) * devices['hp']['initSOC'] / 100) + T_min)
            hp_params['T_in'] = T_in

            process_params['FlexOffer_0'] = hp_params
            params_mod[device] = process_params

    return params_mod

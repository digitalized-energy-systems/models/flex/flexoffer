# FlexOffers and dependency-based FlexOffers after [Neupane et al. (2017)](https://dl.acm.org/doi/10.1145/3077839.3077850) and [Siksnys and Pedersen (2016)](https://dl.acm.org/doi/10.1145/2934328.2934339)

Implementation of the flexibility models presented by [Neupane et al. (2017)](https://dl.acm.org/doi/10.1145/3077839.3077850) and [Siksnys and Pedersen (2016)](https://dl.acm.org/doi/10.1145/2934328.2934339). 

This program allows the FlexOffer- and DFO- calculation of elecrical vehicles and heat pumps and is designed to fit into [unified_flex_scenario](https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario).
 

# Calculate FlexOffers
def calc_flex(OpenTUM_res, FlexOffer_params, t_fac):

    FlexOffers = {}

    for device in FlexOffer_params:

        # FlexOffer calculation ev
        if device == 'ev':

            FlexOffers[device] = {'FlexOffers': float(0)}
            params = FlexOffer_params[device]
            FlexOffer = {}

            # Loop through load processes and calculate FlexOffers
            for idx, process in enumerate(params):

                FO = {'t_es': float(0), 't_ls': float(0), 'p': float(0)}
                t_es = params[process]['t_es']
                t_le = params[process]['t_le']
                e_b = params[process]['e_b']
                e_f = params[process]['e_f']
                phi = params[process]['phi']
                d = params[process]['process_duration']
                p = []

                # Calculate e_min & e_max for each timestep of load process
                for t_step in range(d):

                    e_min = phi[t_step] * e_b
                    e_max = e_min + phi[t_step] * e_f
                    p.append([e_min, e_max])

                # Calculate latest starttime of load process and complete FlexOffer
                t_ls = (t_le + 1) - d
                FO['t_es'] = int(t_es)
                FO['t_ls'] = int(t_ls)
                FO['p'] = p
                FlexOffer['FlexOffer_' + str(idx)] = FO

            # Save results in dict
            FlexOffers[device]['FlexOffers'] = FlexOffer

        # FlexOffer calculation hp
        if device == 'hp':

            FlexOffers[device] = {'FlexOffers': float(0)}
            params = FlexOffer_params[device]
            FlexOffer = {}

            # Loop through hp processes and calculate FlexOffers
            for idx, process in enumerate(params):

                FO = {'t_es': float(0), 't_ls': float(0), 'p': float(0), 'T_in': float(0)}
                thermal_R = params[process]['thermal_R']
                thermal_C = params[process]['thermal_C']
                eta = params[process]['eta']
                T_min = params[process]['T_min']
                T_max = params[process]['T_max']
                T_in = params[process]['T_in']
                T_amb = params[process]['T_amb']
                t_es = 0
                t_le = len(T_amb) - 1
                d = len(T_amb)
                p = []

                # Define calculation variables
                alpha = 1 / (thermal_R * thermal_C)
                beta = 1 / thermal_C
                gamma = 1 / (thermal_R * thermal_C)

                # Calculate e_min & e_max & T_in for each timestep of hp process
                for t_step in range(len(T_amb)):

                    e_min = (T_min - T_in[t_step] + alpha * T_in[t_step] * t_fac - gamma * T_amb[t_step] * t_fac) / \
                            (beta * eta[t_step])
                    e_max = (T_max - T_in[t_step] + alpha * T_in[t_step] * t_fac - gamma * T_amb[t_step] * t_fac) / \
                            (beta * eta[t_step])
                    if e_min < 0:
                        p.append([0, e_max])
                    else:
                        p.append([e_min, e_max])
                    T_in.append(T_in[t_step] - alpha * T_in[t_step] * t_fac + beta * e_min * eta[t_step]
                                + gamma * T_amb[t_step] * t_fac)

                # Calculate latest starttime of load process and complete FlexOffer
                t_ls = (t_le + 1) - d
                FO['t_es'] = int(t_es)
                FO['t_ls'] = int(t_ls)
                FO['p'] = p
                FO['T_in'] = T_in
                FlexOffer['FlexOffer_' + str(idx)] = FO

            # Save results in dict
            FlexOffers[device]['FlexOffers'] = FlexOffer

    return FlexOffers

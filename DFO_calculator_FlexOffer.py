import os
import numpy as np
import pandas as pd
try:
	import gurobipy as gp
	from gurobipy import GRB
except ModuleNotFoundError:
	pass

from pyomo.environ import *


# Gerneration of dependency-based FlexOffers (DFO) from normal FlexOffers.
# Overcomes limitations of simple FlexOffers
def calc_DFO(OpenTUM_res, FlexOffers, FlexOffer_params, t_fac, solver):
    NumSamples = 5

    # Iterate through FlexOffers
    for device in FlexOffers:
        params = FlexOffer_params[device]
        # Calculate DFO's for EV
        if device == 'ev':
            d_DFOs = {}
            # Iterate through FlexOffers of EV
            for index, FO in enumerate(FlexOffers[device]['FlexOffers']):
                controll_var = NumSamples
                d_div = []
                # Define algo variables and DFO dict
                DFO = {}
                d_min = 0

                # Define EV parameters
                p = FlexOffers[device]['FlexOffers'][FO]['p']
                T = len(p)
                cap = OpenTUM_res['devices'][device]['stocap']
                pow_max = OpenTUM_res['devices'][device]['maxpow']
                init_SoC = OpenTUM_res['devices'][device]['initSOC'][index]
                end_SoC = OpenTUM_res['devices'][device]['endSOC'][index]
                eta = OpenTUM_res['devices'][device]['eta']

                # Iterate through timesteps of FlexOffer
                for t_step in range(T):

                    # Define lower and upper bounds for optimisation
                    l_bounds = [bound[0] for bound in p[0:t_step + 1]]

                    if sum(np.subtract(np.array([bound[0] for bound in p]),
                                       np.array([bound[1] for bound in p]))) == 0:
                        u_bounds = [bound[1] for bound in p[0:t_step + 1]]
                    else:
                        u_bounds = [pow_max * t_fac] * len(l_bounds)

                    # Open DataFrame for results
                    V = pd.DataFrame(columns=['d', 'v_min', 'v_max'])

                    # Fill results for t=0 from FlexOffer
                    if t_step == 0:
                        v_min = l_bounds[0]
                        v_max = u_bounds[0]

                        for s in range(NumSamples):

                            if s == 0:
                                V.loc[0] = [0, v_min, v_max]
                            else:
                                V.loc[V.index.max() + 1] = [0, v_min, v_max]

                    # Results calculation for t_step > 0
                    else:
                        # Iterate through number of samples needed
                        for s in range(NumSamples):

                            # Choose depending on previous timesteps
                            if controll_var == NumSamples:
                                d = d_min + s * (d_div[t_step - 1]) / (NumSamples - 1)

                            elif NumSamples > controll_var > 0 and s < controll_var:

                                d = d_min + s * (d_div[t_step - 1] + 0.5 * (d_div[3] - d_div[2])) / (NumSamples - 1)

                                if d > DFO[t_step - 1]['d'][s + 1] + DFO[t_step - 1]['v_min'][s + 1]:
                                    d = DFO[t_step - 1]['d'][s + 1] + DFO[t_step - 1]['v_min'][s + 1]

                            elif s >= controll_var:
                                d = DFO[t_step - 1]['d'][s] + DFO[t_step - 1]['v_min'][s]

                            # Minimization and Maximization to evaluate v_min and v_max for sample
                            if solver == 'gurobi':
                                v_min = min_et_ev_gb(t_step, d, l_bounds, u_bounds, cap, init_SoC, eta, end_SoC)
                                v_max = max_et_ev_gb(t_step, d, l_bounds, u_bounds, cap, init_SoC, eta, end_SoC)
                            else:
                                v_min = min_et_ev(t_step, d, l_bounds, u_bounds, cap, init_SoC, eta, end_SoC)
                                v_max = max_et_ev(t_step, d, l_bounds, u_bounds, cap, init_SoC, eta, end_SoC)

                            # Controll variable needed to avoid flex calc stop after highest operation Plan reached
                            # storage limit
                            if abs(v_min - v_max) < 0.000001 and index == 0:
                                controll_var = s

                            # Save results
                            if s == 0:
                                V.loc[0] = [d, v_min, v_max]
                            else:
                                V.loc[V.index.max() + 1] = [d, v_min, v_max]

                    # Check if V is a convex set of points
                    V = findConvex(V, device, t_step)

                    # Save results
                    DFO[t_step] = V

                    # Redefinition of d_min & d_max for next timestep based on results
                    d_min = min(np.add(np.array(list(V['d']) + list(V['d'])),
                                       np.array(list(V['v_min']) + list(V['v_max']))))
                    d_max = max(np.add(np.array(list(V['d']) + list(V['d'])),
                                       np.array(list(V['v_min']) + list(V['v_max']))))

                    # Controll variable needed to avoid flex calc stop after highest operation Plan reached storage
                    # limit and to get an even profile
                    if controll_var == NumSamples:
                        d_div.append(d_max - d_min)
                    else:
                        d_div.append(d_div[t_step - 1] + 0.5 * (d_div[3] - d_div[2]))

                # Save all DFOs based on FlexOffer
                d_DFOs['DFO_' + str(index)] = DFO

        # Calculate DFOs for HP
        if device == 'hp':

            d_DFOs = {}

            # Iterate through FlexOffers of HP
            for index, FO in enumerate(FlexOffers[device]['FlexOffers']):

                # Define algo variables and DFO dict
                DFO = {}
                d_min = 0
                d_max = 0

                # Define HP parameters
                p = FlexOffers[device]['FlexOffers'][FO]['p']
                T = len(p)
                pow_max = OpenTUM_res['optplan']['HP_ele_run']
                thermal_R = params[FO]['thermal_R']
                thermal_C = params[FO]['thermal_C']
                eta = params[FO]['eta']
                T_min = params[FO]['T_min']
                T_max = params[FO]['T_max']
                T_in = params[FO]['T_in']
                T_amb = params[FO]['T_amb']

                # Iterate through timesteps of FlexOffer
                for t_step in range(T):

                    # Define lower and upper bounds for optimisation
                    l_bounds = [0] * (t_step + 1)
                    u_bounds = [pow_max[t_step] * t_fac] * len(l_bounds)

                    # Open DataFrame for results
                    V = pd.DataFrame(columns=['d', 'v_min', 'v_max'])

                    # Fill results for t=0 from FlexOffer
                    if t_step == 0:
                        v_min = l_bounds[0]
                        v_max = ((T_max - T_in[0]) + (T_in[0] - T_amb[0])
                                 * (1 / (thermal_C * thermal_R)) * t_fac) * thermal_C / eta[0]

                        for s in range(NumSamples):

                            if s == 0:
                                V.loc[0] = [0, v_min, v_max]
                            else:
                                V.loc[V.index.max() + 1] = [0, v_min, v_max]

                    # Results calculation for t_step > 0
                    else:

                        # Iterate through number of samples needed
                        for s in range(NumSamples):

                            # Definition of d based on previous results
                            d = d_min + s * (d_max - d_min) / (NumSamples - 1)

                            # Minimization and Maximization to evaluate v_min and v_max for sample
                            if solver == 'gurobi':
                                v_min = min_et_hp_gb(t_step, d, l_bounds, u_bounds, thermal_R,
                                                     thermal_C, eta, T_min, T_max, T_in, T_amb, t_fac)
                                v_max = max_et_hp_gb(t_step, d, l_bounds, u_bounds, thermal_R,
                                                     thermal_C, eta, T_min, T_max, T_in, T_amb, t_fac)
                            else:
                                v_min = min_et_hp(t_step, d, l_bounds, u_bounds, thermal_R,
                                                  thermal_C, eta, T_min, T_max, T_in, T_amb, t_fac)
                                v_max = max_et_hp(t_step, d, l_bounds, u_bounds, thermal_R,
                                                  thermal_C, eta, T_min, T_max, T_in, T_amb, t_fac)

                            # Save results
                            if s == 0:
                                V.loc[0] = [d, v_min, v_max]
                            else:
                                V.loc[V.index.max() + 1] = [d, v_min, v_max]

                    # Check if V is a convex set of points
                    V = findConvex(V, device, t_step)

                    # Save results
                    DFO[t_step] = V

                    # Redefinition of d_min & d_max for next timestep based on results
                    d_min = min(np.add(np.array(list(V['d']) + list(V['d'])),
                                       np.array(list(V['v_min']) + list(V['v_max']))))
                    d_max = max(np.add(np.array(list(V['d']) + list(V['d'])),
                                       np.array(list(V['v_min']) + list(V['v_max']))))

                    # Save all DFOs based on FlexOffer
                    d_DFOs['DFO_' + str(index)] = DFO

        # Save all DFOs
        FlexOffers[device]['DFOs'] = d_DFOs

    return FlexOffers


# Check if V is a convex set of points, if not => fix
def findConvex(V, device, t_step):

    # check if any v_min > v_max
    for mini in V['v_min']:
        for maxi in V['v_max']:
            if round(mini, 6) > round(maxi, 6):
                print('Non-convex set of points. Check DFO of ' + device
                      + ' ' + 't_step: ' + str(t_step) + 'v_min: ' + str(mini)
                      + 'v_max: ' + str(maxi))

    # check if v_min = convex
    stead_in_min = all(i <= j for i, j in zip(V['v_min'], V['v_min'][1:]))
    stead_de_min = all(i >= j for i, j in zip(V['v_min'], V['v_min'][1:]))

    if not (stead_in_min or stead_de_min):
        print('Possible non-convex set of points at v_min. Check DFO of ' + device
              + 'at t_step: ' + str(t_step))

    # check if v_max = convex
    stead_in_max = all(i <= j for i, j in zip(V['v_max'], V['v_max'][1:]))
    stead_de_max = all(i >= j for i, j in zip(V['v_max'], V['v_max'][1:]))

    if not (stead_in_max or stead_de_max):
        print('Possible non-convex set of points at v_max. Check DFO of ' + device
              + 'at t_step: ' + str(t_step))

    return V


# -----------Pyomo implementation-----------
# find lowest maximum energy amount EV
def min_et_ev(t_step, d, l_b, u_b, c, i_SoC, eta, e_SoC):

    m_min_ev = ConcreteModel()

    # Internal Parameters
    m_min_ev.t_step = Param(initialize=t_step)
    m_min_ev.d = Param(initialize=d)
    m_min_ev.c = Param(initialize=c)
    m_min_ev.i_SoC = Param(initialize=i_SoC)
    m_min_ev.e_SoC = Param(initialize=e_SoC)
    m_min_ev.eta = Param(initialize=eta)
    m_min_ev.t_steps = RangeSet(0, t_step)

    # Bounds
    def lb_rule(m_min_ev, t_var):
        return l_b[t_var]
    m_min_ev.l_b = Param(m_min_ev.t_steps, initialize=lb_rule)

    def ub_rule(m_min_ev, t_var):
        return u_b[t_var]
    m_min_ev.u_b = Param(m_min_ev.t_steps, initialize=ub_rule)

    # Variables
    def bounds_rule(m_min_ev, t_var):
        return (m_min_ev.l_b[t_var], m_min_ev.u_b[t_var])
    m_min_ev.v = Var(m_min_ev.t_steps, bounds=bounds_rule, initialize=0)

    # Constraints
    m_min_ev.constr = ConstraintList()

    m_min_ev.d_constr_range = RangeSet(0, t_step - 1)
    m_min_ev.constr.add(sum(m_min_ev.v[i] for i in m_min_ev.d_constr_range) == m_min_ev.d)

    for i in range(t_step + 1):
        m_min_ev.SoC_constr_range = RangeSet(0, i)
        m_min_ev.constr.add(m_min_ev.e_SoC / 100 + (sum(m_min_ev.v[j] for j in m_min_ev.SoC_constr_range)
                                                    - sum(m_min_ev.l_b[j] for j in m_min_ev.SoC_constr_range))
                            * m_min_ev.eta / m_min_ev.c <= 1)
        m_min_ev.del_component(m_min_ev.SoC_constr_range)

    # Objective
    m_min_ev.obj_range = RangeSet(t_step, t_step)
    m_min_ev.obj = Objective(expr=sum(m_min_ev.v[i] for i in m_min_ev.obj_range), sense=minimize)

    # Solver definition
    solver = SolverFactory('glpk')

    # Solve optimisation problem
    solver.solve(m_min_ev, load_solutions=True)

    # Write model
    m_min_ev.write(os.path.join("flex_models", "FlexOffer", "opt_models", "model_min_ev_pyo.lp"))

    # Get results
    v_min = value(m_min_ev.v[t_step])

    return v_min


# find highest minimum energy amount EV
def max_et_ev(t_step, d, l_b, u_b, c, i_SoC, eta, e_SoC):

    m_max_ev = ConcreteModel()

    # Internal Parameters
    m_max_ev.t_step = Param(initialize=t_step)
    m_max_ev.d = Param(initialize=d)
    m_max_ev.c = Param(initialize=c)
    m_max_ev.i_SoC = Param(initialize=i_SoC)
    m_max_ev.e_SoC = Param(initialize=e_SoC)
    m_max_ev.eta = Param(initialize=eta)
    m_max_ev.t_steps = RangeSet(0, t_step)

    # Bounds
    def lb_rule(m_max_ev, t_var):
        return l_b[t_var]
    m_max_ev.l_b = Param(m_max_ev.t_steps, initialize=lb_rule)

    def ub_rule(m_max_ev, t_var):
        return u_b[t_var]
    m_max_ev.u_b = Param(m_max_ev.t_steps, initialize=ub_rule)

    # Variables
    def bounds_rule(m_max_ev, t_var):
        return (m_max_ev.l_b[t_var], m_max_ev.u_b[t_var])
    m_max_ev.v = Var(m_max_ev.t_steps, bounds=bounds_rule, initialize=0)

    # Constraints
    m_max_ev.constr = ConstraintList()

    m_max_ev.d_constr_range = RangeSet(0, t_step - 1)
    m_max_ev.constr.add(sum(m_max_ev.v[i] for i in m_max_ev.d_constr_range) == m_max_ev.d)

    for i in range(t_step + 1):
        m_max_ev.SoC_constr_range = RangeSet(0, i)
        m_max_ev.constr.add(m_max_ev.e_SoC / 100 + (sum(m_max_ev.v[j] for j in m_max_ev.SoC_constr_range)
                                                    - sum(m_max_ev.l_b[j] for j in m_max_ev.SoC_constr_range))
                            * m_max_ev.eta / m_max_ev.c <= 1)
        m_max_ev.del_component(m_max_ev.SoC_constr_range)

    # Objective
    m_max_ev.obj_range = RangeSet(t_step, t_step)
    m_max_ev.obj = Objective(expr=sum(m_max_ev.v[i] for i in m_max_ev.obj_range), sense=maximize)

    # Solver definition
    solver = SolverFactory('glpk')

    # Solve optimisation problem
    solver.solve(m_max_ev, load_solutions=True)

    # Write model
    m_max_ev.write(os.path.join("flex_models", "FlexOffer", "opt_models", "model_max_ev_pyo.lp"))

    # Get results
    v_max = value(m_max_ev.v[t_step])

    return v_max


# find lowest maximum energy amount HP
def min_et_hp(t_step, d, l_b, u_b, t_R, t_C, eta, T_min, T_max, T_in, T_amb, t_fac):

    # Definition of Heat-Room parameters
    alpha = 1 / (t_R * t_C)
    beta = 1 / t_C
    gamma = 1 / (t_R * t_C)

    m_min_hp = ConcreteModel()

    # Internal Parameters
    m_min_hp.t_step = Param(initialize=t_step)
    m_min_hp.d = Param(initialize=d)
    m_min_hp.T_min = Param(initialize=T_min)
    m_min_hp.T_max = Param(initialize=T_max)
    m_min_hp.t_fac = Param(initialize=t_fac)
    m_min_hp.t_steps = RangeSet(0, t_step)

    # Bounds
    def lb_rule(m_min_hp, t_var):
        return l_b[t_var]
    m_min_hp.l_b = Param(m_min_hp.t_steps, initialize=lb_rule)

    def ub_rule(m_min_hp, t_var):
        return u_b[t_var]
    m_min_hp.u_b = Param(m_min_hp.t_steps, initialize=ub_rule)

    # Variables
    def bounds_rule(m_min_hp, t_var):
        return (m_min_hp.l_b[t_var], m_min_hp.u_b[t_var])
    m_min_hp.v_e = Var(m_min_hp.t_steps, bounds=bounds_rule, initialize=0)

    m_min_hp.v_t = Var(m_min_hp.t_steps, bounds=(m_min_hp.T_min, m_min_hp.T_max))

    # Constraints
    m_min_hp.constr = ConstraintList()

    m_min_hp.d_constr_range = RangeSet(0, t_step - 1)
    m_min_hp.constr.add(sum(m_min_hp.v_e[i] for i in m_min_hp.d_constr_range) == m_min_hp.d)

    for i in range(t_step + 1):
        m_min_hp.t_constr_range0 = RangeSet(i - 1, i - 1)
        m_min_hp.t_constr_range1 = RangeSet(i, i)
        if i == 0:
            m_min_hp.constr.add(sum(m_min_hp.v_t[j] for j in m_min_hp.t_constr_range1)
                                == T_in[i] + sum(m_min_hp.v_e[j] for j in m_min_hp.t_constr_range1)
                                * beta * eta[i] - (T_in[i] * alpha - T_amb[i] * gamma) * t_fac)
        else:
            m_min_hp.constr.add(sum(m_min_hp.v_t[j] for j in m_min_hp.t_constr_range1)
                                == sum(m_min_hp.v_t[j] for j in m_min_hp.t_constr_range0)
                                + sum(m_min_hp.v_e[j] for j in m_min_hp.t_constr_range1)
                                * beta * eta[i] - (sum(m_min_hp.v_t[j] for j in m_min_hp.t_constr_range0)
                                                   * alpha - T_amb[i] * gamma) * t_fac)

        m_min_hp.del_component(m_min_hp.t_constr_range0)
        m_min_hp.del_component(m_min_hp.t_constr_range1)

    # Objective
    m_min_hp.obj_range = RangeSet(t_step, t_step)
    m_min_hp.obj = Objective(expr=sum(m_min_hp.v_e[i] for i in m_min_hp.obj_range), sense=minimize)

    # Solver definition
    solver = SolverFactory('glpk')

    # Solve optimisation problem
    solver.solve(m_min_hp, load_solutions=True)

    # Write model
    m_min_hp.write(os.path.join("flex_models", "FlexOffer", "opt_models", "model_min_hp_pyo.lp"))

    # Get results
    v_min = value(m_min_hp.v_e[t_step])

    return v_min

# find highest minimum energy amount HP


def max_et_hp(t_step, d, l_b, u_b, t_R, t_C, eta, T_min, T_max, T_in, T_amb, t_fac):

    # Definition of Heat-Room parameters
    alpha = 1 / (t_R * t_C)
    beta = 1 / t_C
    gamma = 1 / (t_R * t_C)

    m_max_hp = ConcreteModel()

    # Internal Parameters
    m_max_hp.t_step = Param(initialize=t_step)
    m_max_hp.d = Param(initialize=d)
    m_max_hp.T_min = Param(initialize=T_min)
    m_max_hp.T_max = Param(initialize=T_max)
    m_max_hp.t_fac = Param(initialize=t_fac)
    m_max_hp.t_steps = RangeSet(0, t_step)

    # Bounds
    def lb_rule(m_max_hp, t_var):
        return l_b[t_var]
    m_max_hp.l_b = Param(m_max_hp.t_steps, initialize=lb_rule)

    def ub_rule(m_max_hp, t_var):
        return u_b[t_var]
    m_max_hp.u_b = Param(m_max_hp.t_steps, initialize=ub_rule)

    # Variables
    def bounds_rule(m_max_hp, t_var):
        return (m_max_hp.l_b[t_var], m_max_hp.u_b[t_var])
    m_max_hp.v_e = Var(m_max_hp.t_steps, bounds=bounds_rule, initialize=0)

    m_max_hp.v_t = Var(m_max_hp.t_steps, bounds=(m_max_hp.T_min, m_max_hp.T_max))

    # Constraints
    m_max_hp.constr = ConstraintList()

    m_max_hp.d_constr_range = RangeSet(0, t_step - 1)
    m_max_hp.constr.add(sum(m_max_hp.v_e[i] for i in m_max_hp.d_constr_range) == m_max_hp.d)

    for i in range(t_step + 1):
        m_max_hp.t_constr_range0 = RangeSet(i - 1, i - 1)
        m_max_hp.t_constr_range1 = RangeSet(i, i)
        if i == 0:
            m_max_hp.constr.add(sum(m_max_hp.v_t[j] for j in m_max_hp.t_constr_range1)
                                == T_in[i] + sum(m_max_hp.v_e[j] for j in m_max_hp.t_constr_range1)
                                * beta * eta[i] - (T_in[i] * alpha - T_amb[i] * gamma) * t_fac)
        else:
            m_max_hp.constr.add(sum(m_max_hp.v_t[j] for j in m_max_hp.t_constr_range1)
                                == sum(m_max_hp.v_t[j] for j in m_max_hp.t_constr_range0)
                                + sum(m_max_hp.v_e[j] for j in m_max_hp.t_constr_range1)
                                * beta * eta[i] - (sum(m_max_hp.v_t[j] for j in m_max_hp.t_constr_range0)
                                                   * alpha - T_amb[i] * gamma) * t_fac)

        m_max_hp.del_component(m_max_hp.t_constr_range0)
        m_max_hp.del_component(m_max_hp.t_constr_range1)

    # Objective
    m_max_hp.obj_range = RangeSet(t_step, t_step)
    m_max_hp.obj = Objective(expr=sum(m_max_hp.v_e[i] for i in m_max_hp.obj_range), sense=maximize)

    # Solver definition
    solver = SolverFactory('glpk')

    # Solve optimisation problem
    solver.solve(m_max_hp, load_solutions=True)

    # Write model
    m_max_hp.write(os.path.join("flex_models", "FlexOffer", "opt_models", "model_max_hp_pyo.lp"))

    # Get results
    v_max = value(m_max_hp.v_e[t_step])

    return v_max


# -----------Gurobi API implementation-----------
# find lowest maximum energy amount EV
def min_et_ev_gb(t_step, d, l_b, u_b, c, i_SoC, eta, e_SoC):

    # Creation of model
    model_min_ev = gp.Model('gp_Model')

    # Suppress Gurobi output
    model_min_ev.setParam('Outputflag', 0)

    # Creation of optimisation variables and assignment of bounds
    varis = []

    for t in range(len(l_b)):

        e_vars = model_min_ev.addVar(name='e_var' + str(t), vtype=GRB.CONTINUOUS, lb=l_b[t], ub=u_b[t])
        varis.append(e_vars)

    # Creation of dependency constraint
    model_min_ev.addConstr(sum(varis[0:len(l_b) - 1]) == d)

    # Creation of SoC constraints
    model_min_ev.addConstrs(e_SoC / 100 + (sum(varis[0:t + 1]) - sum(l_b[0:t + 1])) * eta / c <= 1
                            for t in range(len(l_b)))

    # Objective function for minimization
    model_min_ev.setObjective(varis[t_step], GRB.MINIMIZE)

    # Optimisation & writing of model to file
    model_min_ev.update()
    model_min_ev.optimize()
    model_min_ev.write(os.path.join("flex_models", "FlexOffer", "opt_models", "model_min_ev.lp"))

    # If minimization works
    if model_min_ev.status == GRB.OPTIMAL:
        solution = model_min_ev.getVars()
        v_min = solution[t_step].X

    # If minimization doesn't work
    else:
        print('DFO EV Minimization was stopped with status %d' % model_min_ev.status)
        print('t_step:', t_step, 'd:', d)
        v_min = 0

    return v_min

# find highest minimum energy amount EV


def max_et_ev_gb(t_step, d, l_b, u_b, c, i_SoC, eta, e_SoC):

    # Creation of model
    model_max_ev = gp.Model('gp_Model')

    # Suppress Gurobi output
    model_max_ev.setParam('Outputflag', 0)

    # Creation of optimisation variables and assignment of bounds
    varis = []

    for t in range(len(l_b)):

        e_vars = model_max_ev.addVar(name='e_var' + str(t), vtype=GRB.CONTINUOUS, lb=l_b[t], ub=u_b[t])
        varis.append(e_vars)

    # Creation of dependency constraint
    model_max_ev.addConstr(sum(varis[0:len(l_b) - 1]) == d)

    # Creation of SoC constraints
    model_max_ev.addConstrs(e_SoC / 100 + (sum(varis[0:t + 1]) - sum(l_b[0:t + 1])) * eta / c <= 1
                            for t in range(len(l_b)))

    # Objective function for minimization
    model_max_ev.setObjective(varis[t_step], GRB.MAXIMIZE)

    # Optimisation & writing of model to file
    model_max_ev.update()
    model_max_ev.optimize()
    model_max_ev.write(os.path.join("flex_models", "FlexOffer", "opt_models", "model_max_ev.lp"))

    # If maximization works
    if model_max_ev.status == GRB.OPTIMAL:
        solution = model_max_ev.getVars()
        v_max = solution[t_step].X

    # If maximization doesn't work
    else:
        print('DFO EV Maximization was stopped with status %d' % model_max_ev.status)
        print('t_step:', t_step, 'd:', d)
        v_max = 0

    return v_max

# find lowest maximum energy amount HP


def min_et_hp_gb(t_step, d, l_b, u_b, t_R, t_C, eta, T_min, T_max, T_in, T_amb, t_fac):

    # Definition of Heat-Room parameters
    alpha = 1 / (t_R * t_C)
    beta = 1 / t_C
    gamma = 1 / (t_R * t_C)

    # Creation of model
    model_min_hp = gp.Model('gp_Model')

    # Suppress Gurobi output
    model_min_hp.setParam('Outputflag', 0)

    # Creation of optimisation variables and assignment of bounds
    varis = []
    temps = []

    for t in range(len(l_b)):

        e_vars = model_min_hp.addVar(name='e_var' + str(t), vtype=GRB.CONTINUOUS, lb=l_b[t], ub=u_b[t])
        varis.append(e_vars)

    for t in range(len(l_b)):

        t_vars = model_min_hp.addVar(name='t_var' + str(t), vtype=GRB.CONTINUOUS, lb=T_min, ub=T_max)
        temps.append(t_vars)

    # Creation of dependency constraint
    model_min_hp.addConstr(sum(varis[0:len(l_b) - 1]) == d)

    # Creation of Temp constraints
    model_min_hp.addConstr(temps[0] == T_in[0] + varis[0] * beta * eta[0]
                           - (T_in[0] * alpha - T_amb[0] * gamma) * t_fac)
    model_min_hp.addConstrs(temps[t + 1] == temps[t] + varis[t + 1] * beta * eta[t + 1]
                            - (temps[t] * alpha - T_amb[t + 1] * gamma) * t_fac for t in range(len(l_b) - 1))

    # Objective function for minimization
    model_min_hp.setObjective(varis[t_step], GRB.MINIMIZE)

    # Optimisation & writing of model to file
    model_min_hp.update()
    model_min_hp.optimize()
    model_min_hp.write(os.path.join("flex_models", "FlexOffer", "opt_models", "model_min_hp.lp"))

    # If minimization works
    if model_min_hp.status == GRB.OPTIMAL:
        v_min = model_min_hp.getVarByName('e_var' + str(t_step)).X

    # If minimization doesn't work
    else:
        print('DFO HP Minimization was stopped with status %d' % model_min_hp.status)
        print('t_step:', t_step, 'd:', d)
        v_min = 0

    return v_min


# find highest minimum energy amount HP
def max_et_hp_gb(t_step, d, l_b, u_b, t_R, t_C, eta, T_min, T_max, T_in, T_amb, t_fac):

    # Definition of Heat-Room parameters
    alpha = 1 / (t_R * t_C)
    beta = 1 / t_C
    gamma = 1 / (t_R * t_C)

    # Creation of model
    model_max_hp = gp.Model('gp_Model')

    # Suppress Gurobi output
    model_max_hp.setParam('Outputflag', 0)

    # Creation of optimisation variables and assignment of bounds
    varis = []
    temps = []

    for t in range(len(l_b)):

        e_vars = model_max_hp.addVar(name='e_var' + str(t), vtype=GRB.CONTINUOUS, lb=l_b[t], ub=u_b[t])
        varis.append(e_vars)

    for t in range(len(l_b)):

        t_vars = model_max_hp.addVar(name='t_var' + str(t), vtype=GRB.CONTINUOUS, lb=T_min, ub=T_max)
        temps.append(t_vars)

    # Creation of dependency constraint
    model_max_hp.addConstr(sum(varis[0:len(l_b) - 1]) == d)

    # Creation of Temp constraints
    model_max_hp.addConstr(temps[0] == T_in[0] + varis[0] * beta * eta[0]
                           - (T_in[0] * alpha - T_amb[0] * gamma) * t_fac)
    model_max_hp.addConstrs(temps[t + 1] == temps[t] + varis[t + 1] * beta * eta[t + 1]
                            - (temps[t] * alpha - T_amb[t + 1] * gamma) * t_fac for t in range(len(l_b) - 1))

    # Objective function for minimization
    model_max_hp.setObjective(varis[t_step], GRB.MAXIMIZE)

    # Optimisation & writing of model to file
    model_max_hp.update()
    model_max_hp.optimize()
    model_max_hp.write(os.path.join("flex_models", "FlexOffer", "opt_models", "model_max_hp.lp"))

    # If minimization works
    if model_max_hp.status == GRB.OPTIMAL:
        v_max = model_max_hp.getVarByName('e_var' + str(t_step)).X

    # If minimization doesn't work
    else:
        print('DFO HP Maximization was stopped with status %d' % model_max_hp.status)
        print('t_step:', t_step, 'd:', d)
        v_max = 0

    return v_max

from .modify_parameters_FlexOffer import modify_params
from .flex_calculator_FlexOffer import calc_flex
from .DFO_calculator_FlexOffer import calc_DFO

'''FlexOffer flexibility model by B. Neupane et al. (2017) calculates FlexOffers
    for EV's and HP's with device specific algorithms
The extension of FO's to DFO's by Siksnys et al. (2016) overcomes FO-limitations
    by embedding the FO-algorithms into an optimisation environment'''


def flex_FlexOffer(OpenTUM_res, FlexOffer_calc, DFO_calc, solver):
    # Time factor: timesteps per hour
    t_fac = OpenTUM_res['time_data']['t_inval'] / 60
    if FlexOffer_calc:
        # Modify Parameters to fitting format for FlexOffer model
        FlexOffer_params = modify_params(OpenTUM_res, t_fac)

        # Calculate FlexOffers
        FlexOffers = calc_flex(OpenTUM_res, FlexOffer_params, t_fac)

        if DFO_calc:
            # Calculate DFO's from FlexOffers
            FlexOffers = calc_DFO(OpenTUM_res, FlexOffers, FlexOffer_params, t_fac, solver)
    else:
        FlexOffers = 'FlexOffer calculation turned off.'
    return FlexOffers
